import * as React from 'react';

export class SimpleButton extends React.Component {
    render () {
        return (
            <input
                className="btn btn-outline-secondary"
                type="button"
                value="from jsx"
            />
        );
    }
}
