import * as React from 'react';

interface IProps {
    Loader: boolean;
    LoginStatus: boolean;
    LoginIN: Function
}

interface IState {
    login: string;
    pass: string;
}

class FromLogin extends React.Component<IProps,IState> {

    constructor(props: IProps) {
        super(props)

        this.state = {
            login: '',
            pass: ''
        }
    }
    handleUserInput = (e: React.FormEvent<HTMLInputElement>) => {
        const { name, value } = e.currentTarget
        this.setState({
            [name]: value
        }as Pick<IState, keyof IState>)
    }
    hendleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        this.props.LoginIN({name: this.state.login ,password: this.state.pass})
    }
    render() {
        // console.log(this.props)
        // console.log(this.state)
        const { Loader, LoginStatus } = this.props
        const {login, pass} = this.state
        return (
            <form onSubmit={this.hendleSubmit} className="form_login col-md-4 col-sm-12">
                <div className="form_wrapper">
                    {Loader? `ДА` : `not`}
                    {LoginStatus ? `ДА` : `not`}
                    <p>name:'alan',password:'123'</p>
                    <div className="form-group">
                        <input type="text"
                               name="login"
                               className="form-control"
                               placeholder="Login"
                               value={ login }
                               onChange={this.handleUserInput} />
                    </div>
                    <div className="form-group">
                        <input type="password"
                               name="pass"
                               className="form-control"
                               placeholder="Password"
                               value={ pass }
                               onChange={this.handleUserInput} />
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </div>
            </form>
        );
    }
}

export default FromLogin
